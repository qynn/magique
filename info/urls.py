from django.urls import path #, re_path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('safety', views.safety, name='safety'),
    path('contact', views.contact, name='contact'),
    path('chairing', views.chairing, name='chairing'),
]
