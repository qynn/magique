from django.db import models
from radsob.config.constants import c

class Home(models.Model):

    title = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = False)

    info = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    image = models.ImageField(
        upload_to='home/',
        blank = True)

    active = models.BooleanField(default = False)

    def __str__(self):
        return self.title

    def __dic__(self):
        return {'title': self.title,
                'info': self.info,
                'banner': self.image}


class About(models.Model):

    title = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = False)

    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    mandate = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    image = models.ImageField(
        upload_to='about/',
        blank = True)

    active = models.BooleanField(default = False)

    def __str__(self):
        return self.title

    def __dic__(self):
        return {'title': self.title,
                'about': self.about,
                'mandate': self.mandate,
                'banner': self.image}

class Chairing(models.Model):

    title = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = False)

    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    image = models.ImageField(
        upload_to='about/',
        blank = True)

    active = models.BooleanField(default = False)

    def __str__(self):
        return self.title

    def __dic__(self):
        return {'title': self.title,
                'about': self.about,
                'banner': self.image}

class Safety(models.Model):

    title = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = False)

    safety = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    guidelines = models.TextField(
        max_length = 2*c['ABOUT_MAX'],
        blank = True)

    image = models.ImageField(
        upload_to='safety/',
        blank = True)

    active = models.BooleanField(default = False)

    def __str__(self):
        return self.title

    def __dic__(self):
        return {'title': self.title,
                'safety': self.safety,
                'guidelines': self.guidelines,
                'banner': self.image}

class Contact(models.Model):

    title = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = False)

    info = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    email = models.EmailField(
        max_length = c['EMAIL_MAX'],
        blank = True)

    link = models.URLField(
        max_length = c['URL_MAX'],
        blank = True)

    image = models.ImageField(
        upload_to='contact/',
        blank = True)

    active = models.BooleanField(default = False)

    def __str__(self):
        return self.title

    def __dic__(self):
        return {'title': self.title,
                'info': self.info,
                'email': self.email,
                'link': self.link,
                'banner': self.image}


class Social(models.Model):

    title = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = False)

    facebook = models.URLField(
        max_length = c['URL_MAX'],
        blank = True)

    twitter = models.URLField(
        max_length = c['URL_MAX'],
        blank = True)

    # quick fix
    instagram = models.CharField(max_length=c['URL_MAX'],
                                blank=True,
                                help_text="username (w/o @)")
    # instagram = models.URLField(
    #     max_length = c['URL_MAX'],
    #     blank = True)

    mastodon = models.URLField(
        max_length = c['URL_MAX'],
        blank = True)

    gitlab = models.URLField(
        max_length = c['URL_MAX'],
        blank = True)

    active = models.BooleanField(default = False)

    def __str__(self):
        return self.title

    def __dic__(self):
        return {'title': self.title,
                'facebook': self.facebook,
                'twitter': self.twitter,
                'instagram': self.instagram,
                'gitlab': self.gitlab}
