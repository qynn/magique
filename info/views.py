from django.shortcuts import render
from .models import Home, About, Contact, Social, Safety, Chairing
from django.contrib.auth.decorators import login_required
from radsob.urls import ADMIN_URL

def home(request):
    """serves home page """

    template = 'home/home.html'
    home = Home.objects.filter(active=True).first()
    context = home.__dic__() if home else {}
    context['admin'] = ADMIN_URL

    return render(request, template, context)

def about(request):
    """serves about page """

    template = 'about/about.html'
    about = About.objects.filter(active=True).first()
    context = about.__dic__() if about else {}
    context['active'] = "about"

    return render(request, template, context)

# @login_required
def chairing(request):
    """serves chairing page """

    template = 'chairing/chairing.html'
    charing = Chairing.objects.filter(active=True).first()
    context = charing.__dic__() if charing else {}
    context['active'] = "chairing"

    return render(request, template, context)

def safety(request):
    """serves community safety page """
    template = 'safety/safety.html'
    safety = Safety.objects.filter(active=True).first()
    context = safety.__dic__() if safety else {}
    context['active'] = "safety"
    return render(request, template, context)

def contact(request):
    """serves contact page """

    template = 'contact/contact.html'
    contact = Contact.objects.filter(active=True).first()
    context = contact.__dic__() if contact else {}

    social = Social.objects.filter(active=True).first()

    context['social'] = social.__dic__() if social else {}

    # context.update(contact.__dic__() if contact else {})

    source = Contact.objects.filter(title="source").first()
    context['source'] = source.__dic__() if source else {}

    creds = Contact.objects.filter(title="credits").first()
    context['credits'] = creds.__dic__() if creds else {}

    context['active'] = "contact"
    return render(request, template, context)
