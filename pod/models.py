from django.contrib.auth.models import AbstractUser
from django.db import models
# from phone_field import PhoneField
from radsob.config.constants import c

#extending AbstractUser Model
class Pod(AbstractUser):
    pass
    name = models.CharField(max_length=c['NAME_MAX'],
                                blank=False,
                                help_text="required")

    pronouns = models.CharField(max_length=c['NAME_MAX'],
                                blank=False,
                                help_text="required")

    phone = models.CharField(max_length=c['PHONE_MAX'],
                             blank=True,
                             help_text="e.g. 514-666-1234")

    facebook = models.CharField(max_length=c['URL_MAX'],
                                blank=True,
                             help_text="url's username")

    instagram = models.CharField(max_length=c['URL_MAX'],
                                 blank=True,
                             help_text="username (w/o @)")

    twitter = models.CharField(max_length=c['URL_MAX'],
                               blank=True,
                             help_text="handle (w/o @)")

    trusted = models.BooleanField(default = False)

    # class Meta:
    #     ordering = ["-name"]


    def __str__(self):
        return "{n} ({u})".format(u=self.username, n=self.name)

    def __dic__(self):
        return {'id': self.id,
                'username': self.username,
                'name': self.name,
                'pronouns': self.pronouns,
                'phone': self.phone,
                'email': self.email,
                'facebook': self.facebook,
                'instagram': self.instagram,
                'twitter': self.twitter}
