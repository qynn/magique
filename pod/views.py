from django.shortcuts import render, redirect
from django.contrib.auth.decorators import permission_required
from .models import Pod
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from .forms import PodUpdateForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages


@login_required
def pod_index(request):
    """retrieves contact book"""

    template = 'pod/pod_index.html'

    if request.user.is_authenticated:
        pod = Pod.objects.all().order_by('name')
        context = {'title': "pod index",
                   'active': "pod",
                   'pod': [m.__dic__() for m in pod if m.is_active]}
    else:
        context = {}

    return render(request, template, context)


# @permission_required('pod.change_pod', login_url='/accounts/login/')
@login_required
def update_profile(request):
    """access profile update form"""

    m = Pod.objects.get(id=request.user.id) #must exist since logged in
    form = PodUpdateForm(request.POST or None, instance = m)
    template = 'pod/update_profile.html'

    context= {'form': form,
              'title': "profile update",
              'active': "profile",
              'back': "/"}

    if request.method == 'POST':
        if form.is_valid():
            m = form.save(commit= False)
            m.save()
            messages.success(request, "profile successfully updated!")
            return redirect('pod-index')
        else:
            messages.error(request, "something went wrong...")

    return render(request, template, context)


# @permission_required('pod.change_pod', login_url='/accounts/login/')
@login_required
def change_password(request):
    """access password change form"""

    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'password successfully updated!')
            return redirect('update-profile')
        else:
            messages.error(request, 'Please correct the error below:')
    else:
        form = PasswordChangeForm(request.user)

    template = 'pod/change_password.html'
    context =  {'form': form,
                'title': "password modification",
                'back': "/profile"}
    return render(request, template, context)


# Interesting alternative to edit a model via generic views.
# It looks though like the user id must be passed in the url
# which is not what we want here

# class PodUpdateView(UpdateView):
#     model = Pod
#     form_class = PodUpdateForm
#     success_url = '/'
#     template_name = 'pods/podProfile.html'
