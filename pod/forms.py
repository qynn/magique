from django import forms
from .models import Pod
from django.core.validators import validate_email

# Profile Change Form
class PodUpdateForm(forms.ModelForm):

    class Meta:
        model = Pod
        fields = (
            # 'username',
            'name',
            'pronouns',
            'phone',
            'email',
            'facebook',
            'instagram',
            'twitter',)

    # def clean(self):
    #     email = self.cleaned_data.get("email")
    #     if not validate_email(email):
    #         raise forms.ValidationError("Invalid email")
    #     return self.cleaned_data
