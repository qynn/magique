from django.urls import path
from . import views

urlpatterns = [
    # path('', views.pod_page, name='pod-page'),

    path('profile', views.update_profile, name='update-profile'),
    path('profile/password', views.change_password, name='change-password'),

    # Interesting alternative to use generic forms (requires passing key)
    # path('profile/<int:pk>', views.PodUpdateView.as_view(), name='podUpdate'),

    path('pod', views.pod_index, name='pod-index'),
]
