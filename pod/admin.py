from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .models import Pod

# https://saralgyaan.com/posts/how-to-extend-django-user-model-using-abstractuser/

@admin.register(Pod)
class PodAdmin(UserAdmin):
    model = Pod
    list_display = ['username', 'name', 'pronouns', 'email',
                    'id', 'is_active', 'trusted', 'is_staff', 'is_host']
    fieldsets = UserAdmin.fieldsets + (
            (None, {'fields': ('name', 'pronouns', 'phone', 'facebook', 'instagram', 'twitter', 'trusted')}),
    )

    def is_host(self, pod):
        return pod.groups.filter(name = "Hosts").exists()

