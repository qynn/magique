from django.utils import timezone as tz
from datetime import datetime as dt

datetime_format = "%Y-%m-%d %H:%M:%S"
long_datetime_format = "%a %B %-d, %Y - %-I:%M %P"
date_format = "%Y-%m-%d"
h24_time_format = "%H:%M"
ampm_time_format = "%I:%M %P"

def ampm_time_dic(time):
    dic = {'ampm': time.strftime("%p"),
           'hour': time.strftime("%-I"),
           'min': time.strftime("%M")}
    return dic

def short_date_dic(date):
    dic = {'day': date.strftime("%d"),
           'weekday': date.strftime("%a").lower(),
           'month': date.strftime("%b").lower(),
           # 'year': date.strftime("%y")
           'year': date.strftime("%Y")
    }
    return dic

def long_date_dic(date):
    dic = {'day': date.strftime("%d"),
           'weekday': date.strftime("%a"),
           'month': date.strftime("%B"),
           'year': date.strftime("%Y")
        }
    return dic

def convert_str_to_datetime(dt_str, dt_format):
    try:
        print("here")
        datetime = dt.strptime(dt_str, dt_format)
        print("datetime", datetime)
    except Exception as e:
        print("E! convert_str_to_datetime", e)
        datetime = tz.now()
    return datetime

def h24_str_to_time(time_str):
    return convert_str_to_datetime(time_str, h24_time_format)

def ampm_str_to_time(time_str):
    return convert_str_to_datetime(time_str, ampm_time_format)

def local_to_utc_aware_datetime(datetime, local_tz):
    """Converts a local datetime object into UTC timezone-aware datetime """
    local = local_tz.localize(datetime.replace(tzinfo=None))
    return local.astimezone(pytz.utc)

def utc_to_local_aware_datetime(datetime, local_tz):
    """Converts a UTC datetime object into a local timezone-aware datetime """
    return datetime.astimezone(local_tz)

def convert_datetime_to_str(datetime, dt_format):
    """Converts a datetime object into a formatted string"""
    return datetime.strftime(dt_format)
