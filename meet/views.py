from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse
from django.contrib.auth.decorators import permission_required
from radsob.config.constants import c
from django.utils import timezone as tz
from datetime import timedelta
from .utils import short_date_dic
from pod.models import Pod
from .models import Meeting, Poll
from .forms import MeetingUpdateForm, PollForm
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required


def get_visible_meetings():
    return Meeting.objects.filter(visible = True).order_by('date')


def get_meeting(meet_id):
    """ Check if Meeting exists"""
    try:
        meet = Meeting.objects.get(id=meet_id)
        return meet
    except Meeting.DoesNotExist:
        raise Http404("<h1> Invalid Request </h1>")


def paginate(meets, page):
    """
    paginate queryset and return meets for current page
        meets {queryset} -- all objects
        page {int} -- current page to show
    """
    paginator = Paginator(meets, c['MEETS_PER_PAGE'])
    try :
        meets = paginator.page(page)
    except PageNotAnInteger:
        meets = paginator.page(1)
    except EmptyPage:
        meets = paginator.page(paginator.num_pages)
    return meets


@login_required
def all_meetings(request):
    """
    Loads all (visible) upcoming meetings
    """
    meets = get_visible_meetings().filter(date__gte = tz.localtime(tz.now()))

    for m in meets:
        m.date = short_date_dic(m.date)
        # m.start = ampm_time_dic(m.start)
        # m.end = ampm_time_dic(m.end)

    page = request.GET.get('page', 1) #parse slug
    meets = paginate(meets, page)
    context = {'meets': meets, 'active': "meet", 'title': "schedule"}

    return render(request, 'meet/all_meetings.html', context)


@permission_required('meet.change_meeting', login_url='/accounts/login/')
def update_meeting(request, id):
    """access meeting update form"""

    m = get_meeting(id)
    form = MeetingUpdateForm(request.POST or None, instance = m)
    template = 'meet/update_meeting.html'

    context= {'form': form,
              'title': "meeting update",
              'date_min': tz.localtime(tz.now()),
              'active': "meet",
              'back': "/schedule"}

    if request.method == 'POST':
        if form.is_valid():
            m = form.save(commit= False)
            m.admin = Pod.objects.get(id=request.user.id)
            m.save()
            messages.success(request, "event successfully updated!")
            return redirect('all-meetings')
        else:
            messages.error(request, "something went wrong...")

    return render(request, template, context)


@permission_required('meet.add_meeting', login_url='/accounts/login/')
def new_meeting(request):
    """access create meeting form"""

    m = Meeting()
    form = MeetingUpdateForm(request.POST or None, instance = m)
    template = 'meet/update_meeting.html'

    context= {'form': form,
              'title': "new meeting",
              'date_min': tz.localtime(tz.now()),
              'active': "meet",
              'back': "/schedule"}

    if request.method == 'POST':
        if form.is_valid():
            m = form.save(commit= False)
            m.admin = Pod.objects.get(id=request.user.id)
            m.save()
            messages.success(request, "meeting successfully created!")
            return redirect('all-meetings')
        else:
            messages.error(request, "something went wrong...")

    return render(request, template, context)


@permission_required('meet.add_meeting', login_url='/accounts/login/')
def copy_meeting(request):
    """duplicate last meeting and redirect to meeting form"""

    m = Meeting.objects.latest('date')
    m.pk = None #cloning object
    m.host = None
    m.admin = Pod.objects.get(id=request.user.id)
    m.date = m.date + timedelta(days=7) # 1 week later
    m.save()

    # return redirect("/meeting/{i}/update".format(i=m.id))
    return redirect("/schedule")

# @login_required
def poll(request):
    """ loads polling form """

    form = PollForm()
    context = {'form': form, 'active': "meet", 'title': "my availability"}

    return render(request, 'meet/poll.html', context)

# @login_required
def vote(request):
    """ handles post request from /poll form """

    if request.method == 'POST':
        form = PollForm(request.POST)
        if form.is_valid():

            options = Poll.objects.filter(visible = True).order_by('datetime')
            d = form.cleaned_data
            print("d", d)
            for o in options:
                try:
                    if d["_" + str(o.id)]:
                        print("o.score", o.score)
                        o.score += 1
                        o.save()
                        print("o.score", o.score)
                except KeyError:
                    messages.error(request, "something went wrong...")
                    return redirect("/poll")


    messages.success(request, 'form successfully submitted!')
    return redirect("/")
