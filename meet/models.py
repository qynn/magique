from django.db import models
from radsob.config.constants import c
from django.utils import timezone as tz
from pod.models import Pod
from .utils import ampm_time_dic, short_date_dic, long_date_dic
# from sortedm2m.fields import SortedManyToManyField

# def is_organizer(user):
    # return user.groups.filter(name='Organizers').exists()

class Meeting(models.Model):

    date = models.DateField(
        blank=False,
        default=tz.now)

    start = models.TimeField(
        blank=False,
        auto_now=False,
        default="2:00 PM")

    end = models.TimeField(
        blank=True,
        auto_now=False,
        default="3:00 PM")

    location = models.CharField(
        max_length = c['TITLE_MAX'],
        blank = True)

    link = models.URLField(
        max_length = c['URL_MAX'],
        blank = True)

    host = models.ForeignKey(Pod,
        blank=True,
        null=True,
        related_name='host',
        on_delete = models.PROTECT)

    about = models.TextField(
        max_length = c['ABOUT_MAX'],
        blank = True)

    visible = models.BooleanField(default = True)

    admin = models.ForeignKey(Pod, # i.e. 'created_by'
        related_name='admin',
        blank=True, #will be filled automatically
        on_delete = models.PROTECT)

    def __str__(self):
        return self.date.strftime("%Y-%m-%d %I:%M %P")

class Poll(models.Model):

    datetime = models.DateTimeField(
        blank=False,
        default=tz.now)

    score = models.IntegerField(
        default=0)

    visible = models.BooleanField(default = True)

    def __str__(self):
        return tz.localtime(self.datetime).strftime("%A %-I:%M %P")
