$(document).ready(function(){

    startTime = $("#id_start").val();
    endTime = $("#id_end").val();

    $("#id_start").click(function() {
        hideKeyboard();
    });

    $("#id_end").click(function() {
        hideKeyboard();
    });

    minTime = "10:00 AM";
    maxTime = "8:00 PM";

    // https://timepicker.co/options/

    $('#id_start').timepicker({
        timeFormat: 'h:mm p', //AM/PM
        // timeFormat: 'H:mm', //24hour
        interval: 30,
        minTime: minTime,
        maxTime: maxTime,
        defaultTime: startTime,
        startTime: minTime,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#id_end').timepicker({
        timeFormat: 'h:mm p', //AM/PM
        // timeFormat: 'H:mm', //24hour
        interval: 30,
        minTime: minTime,
        maxTime: maxTime,
        defaultTime: endTime,
        startTime: minTime,
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

 });

function  hideKeyboard(){

    // display hidden field it in order to focus it
    hidden = $("#date_min");
    hidden.attr('style', 'display:true;');
    hidden.focus();
    // hide it again after a short delay

    setTimeout(function() {
        hidden.attr('style', 'display:none;');
    }, 50);


}
