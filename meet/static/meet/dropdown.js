
$(document).ready(function(){

    //styling for all select options for host
    $("#id_host option").each(function(){
        // option elements are OS-dependent and are not part of the HTML/browser
        // they cannot be styled via CSS

    });

    //adding padding to all inputs
    $("#meeting-form :input").each(function(){
        $(this).css("padding-left", "0.5rem");
    });

    $('.dropdown').click(function(){

            var btn_id = $(this).attr('id');
            var form_id = "form_" + btn_id.substring(4, btn_id.length);

            if(!($("#" + form_id).hasClass("show"))){ //inverted behavior
                $(this).addClass("show");
            }
            else{
                $(this).removeClass("show");
            }

        });
    // });


});
