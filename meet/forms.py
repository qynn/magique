from django import forms
from django.utils import timezone as tz
from .models import Meeting, Poll

class MeetingUpdateForm(forms.ModelForm):

    class Meta:
        model = Meeting
        fields = (
            'date',
            'start',
            'end',
            'location',
            'link',
            'host',
            'about',
            'visible',)

    # def clean(self):
    #     admin = self.cleaned_data.get("admin")
    #     print("admin", admin)
    #     return self.cleaned_data

class PollForm(forms.Form):

    # WARNING: it looks like server must be restarted for this to reload!!
    options = Poll.objects.filter(visible=True).order_by('datetime')
    for o in options:
        label = tz.localtime(o.datetime).strftime("%A %-I:%M %P")
        exec("%s = %s" % ("_" + str(o.id), "forms.BooleanField(label=label, required=False)"))
