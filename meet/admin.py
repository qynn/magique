from django.contrib import admin
from django import forms
from datetime import timedelta, datetime
from django.utils import timezone as tz

from pod.models import Pod
from .models import Meeting, Poll

class HostFilter(admin.SimpleListFilter):
    title = 'Host'
    parameter_name = 'host'
    default_value = None

    def lookups(self, request, model_admin):
        lookupList = []
        for host in Pod.objects.all():
            lookupList.append((str(host.id), host.name))
        return sorted(lookupList, key=lambda tp: tp[1])

    def queryset(self, request, queryset):
        value = super(HostFilter, self).value()
        if value is not None: # None is All
            host_id = Pod.objects.get(pk=value).id
            return queryset.filter(host = host_id)
        return queryset


class DateFilter(admin.SimpleListFilter):
    title = 'Date'
    parameter_name = 'date'
    default_value = '0' #upcoming

    def lookups(self, request, model_admin):
        lookupList = [('0', "Upcoming"),
                      ('1', "Past")]
        return lookupList

    def queryset(self, request, queryset):
        value = super(DateFilter, self).value()
        if value is not None: # None is All
            if value  =='0': #upcoming
                return queryset.filter(date__gte = tz.localtime(tz.now()))
            elif value == '1': #past
                return queryset.filter(date__lt = tz.localtime(tz.now()))
        return queryset


@admin.register(Meeting)
class HostAdmin(admin.ModelAdmin):
    list_display = ('date', 'start', 'location', 'host', 'id', 'visible')
    list_filter = (DateFilter, HostFilter)
    exclude = ('admin',)
    ordering = ('-date',)

    def save_model(self, request, obj, form, change):
        obj.admin = request.user
        super().save_model(request, obj, form, change)

    def duplicate(self, request, queryset):
        for m in queryset:
            m.pk = None #cloning object
            m.host = None
            m.admin = Pod.objects.get(id=request.user.id)
            m.date = m.date + timedelta(days=7) # 1 week later
            m.save()
    actions = [duplicate]


@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    list_display = ('option', 'datetime', 'score', 'id', 'visible')
    ordering = ('-datetime',)

    def option(self, option):
        return tz.localtime(option.datetime).strftime("%A %-I:%M %P")
    # option.short_description = "option"

    def clear_scores(self, request, queryset):
        for o in queryset:
            o.score = 0
            o.save()

    def toggle_visibility(self, request, queryset):
        for o in queryset:
            o.visible = not o.visible
            o.save()

    def hide_options(self, request, queryset):
        for o in queryset:
            o.visible = False
            o.save()

    actions = [clear_scores, toggle_visibility]
