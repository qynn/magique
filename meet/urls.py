from django.urls import path
from . import views

urlpatterns = [
    path('meeting/<str:id>/update', views.update_meeting, name='update-meeting'),
    path('meeting/new', views.new_meeting, name='new-meeting'),
    path('meeting/copy', views.copy_meeting, name='copy-meeting'),
    path('schedule', views.all_meetings, name='all-meetings'),
    path('poll', views.poll, name='poll'),
    path('vote', views.vote, name='vote'),
]
