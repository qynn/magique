# Generated by Django 3.0.4 on 2020-11-14 16:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meet', '0003_auto_20201113_1624'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meeting',
            name='location',
            field=models.CharField(blank=True, max_length=60),
        ),
    ]
