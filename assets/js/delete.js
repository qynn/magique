$(document).ready(function(){
    addListnerModal();
});
function addListnerModal(){
    var csrftoken = getCookie('csrftoken');
    $('#popup').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        console.log(button)
        var pageId= button.data('pageid'); // Extract info from data-* attributes

        
        var modal = $(this);
        console.log("popup");
        if(pageId == undefined){
            var nbDataPoints = button.data('nbentry');
            var pageId = button.data('boardid');
            var text = "This action will permanently delete " + nbDataPoints + " individual data points."
            $(".modal-body div").html(text.bold())
            //modal.append(divText)
        }
        modal.find('.modal-footer .choice-ok').val(pageId);
    })

    $('#popup .modal-footer .choice-ok').on('click', function(){

        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });

        $.ajax({
            data: $(this).val(),
            url: "delete",
            method: "POST",
            success: function(data){
                window.location.reload(forceGet = true);
            }
        })

    })

}
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
