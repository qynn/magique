from decouple import config

IS_LOCAL = config('IS_LOCAL', cast=bool, default=False)

if (IS_LOCAL): #use config/local.py
    from radsob.config.local import *
    print("*W*A*R*N*I*N*G* using LOCAL settings")
else: #use qonfig/production.py
    from radsob.config.production import *
    print("using PROD settings")
