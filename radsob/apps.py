from django.apps import AppConfig

class Pod(AppConfig):
    name = 'pod'

class Meet(AppConfig):
    name = 'meet'

class Info(AppConfig):
    name = 'info'
