from .base import *
import dj_database_url
from decouple import config

# W! Never use DEBUG=True in production
DEBUG = False
print("* *DEBUG* *\t", DEBUG)

ALLOWED_HOSTS = ['www.magique.club', 'magique.club']
#ALLOWED_HOSTS = ['*']
print("ALLOWED_HOSTS\t", ALLOWED_HOSTS)

# MIDDLEWARE_CLASSES = (
#         'whitenoise.middleware.WhiteNoiseMiddleware',
# )
