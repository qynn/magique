from pytz import timezone

c={} #Constants dictionnary
# Models
c['NAME_MAX'] = 20
c['TITLE_MAX'] = 60
c['ABOUT_MAX'] = 2000
c['URL_MAX'] = 128
c['EMAIL_MAX'] = 40
c['PHONE_MAX'] = 15

# Media
c['MEDIA_ERROR'] = '/media/error.jpg'
c['PIC_SIZE'] = 1000
c['PIC_QUALITY'] = 80
c['THUMB_SIZE'] = 300
c['THUMB_QUALITY'] = 75

# HTML
c['MEETS_PER_PAGE'] = 8

# Venue
c['ACCESSIBILITY'] = (
        ('0', 'No accessibility info'),
        ('1', 'Flight(s) of Stairs'),
        ('2', 'Wheel Chair Accessible'),
        ('3', 'Assistance Available'),
    )
# Gig
c['COVER'] = (
        ('0', None),
        ('1', 'PWYC'),
        ('2', 'NOTAFLOF'),
        ('3', 'Sliding Scale'),
        ('4', 'Suggested'),
        ('5', 'Fundraiser'),
    )

c['TIME_ZONE'] = timezone('America/Toronto')
