import os
# https://docs.djangoproject.com/en/3.0/topics/logging/

def get_logging_config(BASE_DIR):
    """ Arguments: BASE_DIR -- app root directory"""

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False}

    LOGGING['formatters'] = {
        'verbose': {
            'format': '\n{levelname} {asctime} {name}.{funcName}:\n{message}',
            'style': '{',
        },
        'short': {
            'format': '\n{levelname} - {message}',
            'style': '{',
        },
    }

    LOGGING['filters'] = {
        'require_debug_true':{
            '()': 'django.utils.log.RequireDebugTrue',
            },
    }

    LOGGING['handlers'] = {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'info': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'logging/info.log'),
            'formatter': 'short',
        },
        'debug': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'logging/debug.log'),
            'formatter': 'verbose',
        },
    }

    LOGGING['loggers'] = {
        'django': {
            'handlers': ['console'],
            #set DJANGO_LOG_LEVEL to DEBUG to see all database queries..
            'level': os.getenv('DJANGO_LOG_LEVEL', 'WARNING'),
            'propagate': False,
        },
        'info': {
            'handlers': ['console', 'info', 'debug'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'pod': {
            'handlers': ['console', 'info', 'debug'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'meet': {
            'handlers': ['console', 'info', 'debug'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }

    return LOGGING
