# import django_heroku
from decouple import config
import os
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

#see ../settings.py
print("* *DEBUG* *\t", DEBUG)

ALLOWED_HOSTS = ['192.168.2.197', '127.0.0.1', 'localhost']
print("ALLOWED_HOSTS\t", ALLOWED_HOSTS)

# debug toolbar
INTERNAL_IPS = ('127.0.0.1', '192.168.2.197')

MIDDLEWARE += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
)
DEBUG_TOOLBAR_PANELS = [
    # 'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
    'debug_toolbar.panels.profiling.ProfilingPanel',
]
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

# heroku local
# django_heroku.settings(locals())
